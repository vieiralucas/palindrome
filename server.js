const express = require('express');
const _ = require('lodash');

const app = express();

const getChars = phrase => _.chain(phrase)
  .deburr()
  .words()
  .join('')
  .split('')
  .value();

app.get('/api/is-palindrome', (req, res) => {
  const { phrase } = req.query;

  if (!phrase) {
    res.status(400).json({ error: 'Missing required parameter phrase' });
    return;
  }

  const backwards = getChars(phrase).reverse().join('').toLowerCase();
  const toCompare = getChars(phrase).join('').toLowerCase();

  if (toCompare === backwards) {
    res.json({ message: `${phrase} is a palindrome` });
    return;
  }

  res.status(400).json({ message: `${phrase} is not a palindrome` });
});

app.listen(4000, () => {
  console.info('Express server listening at 4000');
});

module.exports = app;
