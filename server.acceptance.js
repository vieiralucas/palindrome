const { expect } = require('chai');
const request = require('supertest');

const app = require('./server');

describe('GET /api/is-palindrome', () => {
  describe('when no phrase query parameter is provided', () => {
    it('should respond 400', () => {
      return request(app)
        .get('/api/is-palindrome')
        .expect(400);
    });

    it('should send descriptive error', () => {
      return request(app)
        .get('/api/is-palindrome')
        .then(({ body }) => {
          expect(body).to.have.property('error').equal('Missing required parameter phrase');
        });
    });
  });

  describe('when phrase is not a palindrome', () => {
    const inputs = [
      'MACACO',
      'This is not a palindrome too',
      'Neither this one'
    ];

    it('should send 400', () => {
      const tests = inputs.map(i =>
        request(app).get(`/api/is-palindrome?phrase=${i}`).expect(400));

      return Promise.all(tests)
    });

    it('shouls send descriptive message', () => {
      const tests = inputs.map(i => request(app).get(`/api/is-palindrome?phrase=${i}`)
        .then(({ body }) => {
          expect(body).to.have.property('message').equal(`${i} is not a palindrome`);
        }));

      return Promise.all(tests)
    });
  });

  describe('when phrase is a palindrome', () => {
    const inputs = [
      'ABA',
      'A MAN A PLAN A CANAL PANAMA', // this handles ignoring spaces
      'A dog! A panic in a pagoda!'
    ];

    it('should send 200', () => {
      const tests = inputs.map(i =>
        request(app).get(`/api/is-palindrome?phrase=${i}`).expect(200));

      return Promise.all(tests);
    });

    it('shouls send descriptive message', () => {
      const tests = inputs.map(i => request(app).get(`/api/is-palindrome?phrase=${i}`)
        .then(({ body }) => {
          expect(body).to.have.property('message').equal(`${i} is a palindrome`);
        }));

      return Promise.all(tests);
    });
  });
});
