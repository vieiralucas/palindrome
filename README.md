Palindrome Checker
==================

Running
-------

### Dependencies

-   [Node](http://nodejs.org/) (I recommend using [NVM](https://github.com/creationix/nvm))

Installing NVM

    $ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash

Installing NodeJS v6

    $ nvm install 6
    $ nvm use 6

    # you can make 6 the default version
    # so you don't have to switch to it every time you open a shell session
    $ nvm alias default 6

Install NPM dependencies

    $ npm install

Run!

    $ npm start

Tests!

    # acceptance for api
    $ npm run acceptance

TODO

Add tests to react app
