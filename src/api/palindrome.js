import 'whatwg-fetch'
const qs = require('querystring');

const check = phrase => {
  const query = qs.stringify({
    phrase
  });

  return fetch(`/api/is-palindrome?${query}`)
    .then(response => {
      return response.json()
        .then(json => {
          if (!response.ok) {
            return Promise.reject({ message: json.message });
          }

          return json;
        });
    });
};

export default {
  check
};
