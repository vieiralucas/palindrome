import React, { Component } from 'react';
import './PalindromeForm.css';

class PalindromeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phrase: ''
    };
  }

  handleChange = event => {
    this.setState({ phrase: event.target.value });
  }

  onFormSubmit = event => {
    event.preventDefault();

    this.props.onSubmit(this.state.phrase);
  }

  render() {
    return (
      <form className='palindrome-form' onSubmit={this.onFormSubmit}>
        <input type='text'
          placeholder={'Insert a phrase here to check if it is a palindrome'}
          value={this.state.phrase} onChange={this.handleChange} disabled={this.props.loading}
        />
        <button type='submit' disabled={this.props.loading || this.state.phrase.length <= 0}>Check!</button>
      </form>
    );
  }
}

export default PalindromeForm;
