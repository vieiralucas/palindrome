import React from 'react';
import './PalindromeMessage.css';

const PalindromeMessage = ({ isPalindrome, message }) => (
  <div className='palindrome-message'>
    { isPalindrome &&
      <p className='success'>{message}</p>
    }
    { !isPalindrome &&
      <p className='danger'>{message}</p>
    }
  </div>
);

export default PalindromeMessage;
