import React, { Component } from 'react';
import './App.css';

import PalindromeForm from './PalindromeForm';
import PalindromeMessage from './PalindromeMessage';

import palindrome from '../api/palindrome';

class App extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      message: '',
      isPalindrome: null
    };
  }

  checkPalindrome = phrase => {
    this.setState({
      loading: true
    });

    palindrome.check(phrase)
      .then(({ message }) => {
        this.setState({
          message,
          isPalindrome: true
        });
      })
      .catch(({ message }) => {
        this.setState({
          message,
          isPalindrome: false
        });
      })
      .then(() => {
        this.setState({
          loading: false
        });
      });
  }

  render() {
    return (
      <div className="app">
        <PalindromeForm onSubmit={this.checkPalindrome} loading={this.state.loading}/>
        { this.state.isPalindrome !== null &&
          <PalindromeMessage isPalindrome={this.state.isPalindrome} message={this.state.message} />
        }
      </div>
    );
  }
}

export default App;
